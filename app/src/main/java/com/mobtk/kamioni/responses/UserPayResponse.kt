package com.mobtk.kamioni.responses

import com.google.gson.annotations.SerializedName
import com.mobtk.kamioni.models.UserPayModel

class UserPayResponse(@SerializedName("pagos") var payUser: ArrayList<UserPayModel>)