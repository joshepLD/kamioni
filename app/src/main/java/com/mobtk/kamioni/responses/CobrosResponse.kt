package com.mobtk.kamioni.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mobtk.kamioni.models.CobrosModel

data class CobrosResponse (@SerializedName("cobros") @Expose val cobros: MutableList<CobrosModel>)