package com.mobtk.kamioni.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mobtk.kamioni.models.PublicacionModel

data class PublicacionesResponse (@SerializedName("publicaciones") @Expose val publicaciones: ArrayList<PublicacionModel>,
                         @SerializedName("valido") @Expose val valido:String)