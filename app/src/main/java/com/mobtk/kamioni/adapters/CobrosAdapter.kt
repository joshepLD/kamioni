package com.mobtk.kamioni.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.kamioni.R
import com.mobtk.kamioni.databinding.ItemCobrosBinding
import com.mobtk.kamioni.models.CobrosModel

class CobrosAdapter(private val items: MutableList<CobrosModel>) : RecyclerView.Adapter<CobrosAdapter.ViewHolder>() {
    private var contexto: Context? =null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto=parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCobrosBinding.inflate(inflater,parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])

        if (items[position].tipo=="1"){
            holder.binding.ivStatus.setImageDrawable(contexto?.resources?.getDrawable(R.drawable.ic_mastercard,contexto?.theme))
        }else{
            holder.binding.ivStatus.setImageDrawable(contexto?.resources?.getDrawable(R.drawable.ic_visa,contexto?.theme))
        }
    }

    inner class ViewHolder(val binding: ItemCobrosBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(cobrosModel: CobrosModel) {
            binding.elemento= cobrosModel
            binding.executePendingBindings()
        }
    }
}