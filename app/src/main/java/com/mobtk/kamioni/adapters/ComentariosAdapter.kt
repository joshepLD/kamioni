package com.mobtk.kamioni.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobtk.kamioni.R
import com.mobtk.kamioni.activities.DetallePublicacionProActivity
import com.mobtk.kamioni.activities.PerfilUsers
import com.mobtk.kamioni.fragments.PerfilFragment
import com.mobtk.kamioni.models.ComentariosModel
import kotlinx.android.synthetic.main.activity_detalle_publicacion_pro.*
import kotlinx.android.synthetic.main.item_comentarios.view.*


class ComentariosAdapter(val comentarios: ArrayList<ComentariosModel>) :
    RecyclerView.Adapter<ComentariosAdapter.ComentariosViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComentariosViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_comentarios, parent, false)
        context=parent.context
        return ComentariosViewHolder(view)
    }

    override fun getItemCount(): Int {
        return comentarios.size
    }

    override fun onBindViewHolder(holder: ComentariosViewHolder, position: Int) {
        val itemComentarios = comentarios[position]
        var activity: AppCompatActivity = holder.itemView.context as DetallePublicacionProActivity
        holder.itemView.tvNombreUser.setOnClickListener {
            val intent = Intent(holder.itemView.context, PerfilUsers::class.java)
            holder.itemView.context?.startActivity(intent)
        }

        holder.bindComentarios(itemComentarios)
    }

    inner class ComentariosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindComentarios(comentariosModel: ComentariosModel) {
            val requestManager = Glide.with(context)
            val requestBuilder = requestManager.load(comentariosModel.fotoUser)
            requestBuilder.into(itemView.imgeUser)
            itemView.tvNombreUser.text = comentariosModel.nameUser
            itemView.tvComentarios.text = comentariosModel.comentario

        }
    }

}