package com.mobtk.kamioni.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.kamioni.R
import com.mobtk.kamioni.models.UserPayModel
import com.mobtk.kamioni.databinding.ItemPayUserBinding

class PayUserAdapter(private val items: List<UserPayModel>): RecyclerView.Adapter<PayUserAdapter.ViewHolder>() {
    private var contexto: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto = parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemPayUserBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])

        if (items[position].payCard == "1"){
            holder.binding.ivStatus.setImageDrawable(contexto?.resources?.getDrawable(
                R.drawable.ic_mastercard, contexto?.theme))
        } else {
            holder.binding.ivStatus.setImageDrawable(contexto?.resources?.getDrawable(
                R.drawable.ic_visa, contexto?.theme))
        }
    }

    inner class ViewHolder(val binding: ItemPayUserBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(payModel: UserPayModel){
            binding.elemento = payModel
            binding.executePendingBindings()
        }
    }
}