package com.mobtk.kamioni.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserPayModel (@SerializedName("nombre") @Expose val nameUser: String,
                    @SerializedName("apellidos") @Expose val lastNameUser: String,
                    @SerializedName("servicio") @Expose val serviceUser: String,
                    @SerializedName("cantidad") @Expose val cantidadUser: String,
                    @SerializedName("pago_tarjeta") @Expose val payCard: String,
                    @SerializedName("fechaPago") @Expose val datePay: String)