package com.mobtk.kamioni.models

import com.google.gson.annotations.SerializedName

data class PublicacionModel
    (@SerializedName("publicacion_id") var id_publicacion:String?=null,
     @SerializedName("publicacion_user") var user_publicacion:String?=null,
     @SerializedName("publicacion_tipo") var tipo_publicacion:String?=null,
     @SerializedName("publicacion_titulo") var titulo_publicacion:String?=null,
     @SerializedName("publicacion_img") var img_publicacion:String?=null,
     @SerializedName("publicacion_desc") var description_publicacion:String?=null,
     @SerializedName("publicacion_fecha") var date_publicacion:String?=null,
     @SerializedName("publicacion_contac") var contact_publicacion:String?=null,
     @SerializedName("publicacion_patrocinada") var  promoted_publicacion:String="0"
     )