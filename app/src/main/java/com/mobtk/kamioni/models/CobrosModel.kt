package com.mobtk.kamioni.models

import com.google.gson.annotations.SerializedName

class CobrosModel (@SerializedName("nombre")val nombre:String,
                               @SerializedName("correo")val correo:String,
                               @SerializedName("cantidad")var cantidad:String,
                               @SerializedName("fechaPago")val fechaPago:String,
                               @SerializedName("tipo")val tipo:String)