package com.mobtk.kamioni.models


data class Notificacion (val title: String?, val alert: String?, val datos: MutableMap<String,String>?)
