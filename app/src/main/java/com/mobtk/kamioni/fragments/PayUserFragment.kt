package com.mobtk.kamioni.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobtk.kamioni.R
import com.mobtk.kamioni.models.UserPayModel
import com.mobtk.kamioni.adapters.PayUserAdapter
import com.mobtk.kamioni.databinding.FragmentPayUserBinding
import com.mobtk.kamioni.taks.UserTask
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class PayUserFragment : Fragment() {
    private lateinit var binding: FragmentPayUserBinding
    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var pay:List<UserPayModel>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_pay_user, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pay = ArrayList()
        getPayUser()
    }

    private fun getPayUser(){
        doAsync {
            val payU = UserTask.getPayUsers()
            uiThread {
                    linearLayoutManager = LinearLayoutManager(context)
                    binding.rvPayUser.layoutManager =linearLayoutManager
                    pay = payU.payUser
                    binding.rvPayUser.adapter = PayUserAdapter(pay)

            }
        }
    }
}