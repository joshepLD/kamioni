package com.mobtk.kamioni.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.mobtk.kamioni.R
import com.mobtk.kamioni.utilities.SharedPreference
import com.mobtk.kamioni.utilities.ViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_profile_provedor.*

class PerfilFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_provedor, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUserProfile()


    }
    fun setUserProfile(){
        val requestManager = Glide.with(this)
        val requestBuilder = requestManager.load("https://i1.wp.com/geekazos.com/wp-content/uploads/2015/02/fb2.jpg?fit=1280%2C720")
        requestBuilder.into(iv_profileEmpresa)
        tvNameProfileEmpresa.text="Usuario"

        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFragment(PublicacionesFragment(), "Mis\npublicacione")
        adapter.addFragment(PayUserFragment(), "Pagos")
        adapter.addFragment(CobrosFragment(), "Cobros")
        viewPagerE.adapter = adapter

        tabsUEmpresa.setupWithViewPager(viewPagerE)
    }
}