package com.mobtk.kamioni.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.cometchat.pro.models.Conversation
import com.cometchat.pro.models.User
import com.mobtk.kamioni.ChatActivity
import com.mobtk.kamioni.R
import listeners.ConversationItemClickListener
import screen.CometChatConversationListScreen


class ChatListFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        CometChatConversationListScreen.setItemClickListener(object : ConversationItemClickListener() {
            override fun setItemClickListener(conversation: Conversation, position: Int) {
                super.setItemClickListener(conversation, position)
                val user = conversation.lastMessage.sender as User
                val uid = user.uid
                val avatar = user.avatar
                val name = user.avatar
                val status = user.status

                val intent = Intent(context,ChatActivity::class.java)
                intent.putExtra("userid",uid)
                intent.putExtra("username",name)
                intent.putExtra("avatar",avatar)
                intent.putExtra("status",status)
                startActivity(intent)
            }

            override fun setItemLongClickListener(
                conversation: Conversation,
                position: Int
            ) {
                super.setItemLongClickListener(conversation, position)
            }
        })

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat_list, container, false)
    }

}
