package com.mobtk.kamioni.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobtk.kamioni.R
import com.mobtk.kamioni.activities.CreatePublicationActivity
import com.mobtk.kamioni.adapters.PublicacionAdapter
import com.mobtk.kamioni.databinding.FragmentPublicacionesBinding
import com.mobtk.kamioni.models.PublicacionModel
import com.mobtk.kamioni.taks.PublicacionesTask
import kotlinx.android.synthetic.main.fragment_publicaciones.*
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception


class PublicacionesFragment : Fragment() {

    private lateinit var binding: FragmentPublicacionesBinding
    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var publicaciones: ArrayList<PublicacionModel>
    private var animation : LayoutAnimationController? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_publicaciones, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(binding.root, savedInstanceState)

        animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation)
        binding.rvPublicaciones.layoutAnimation=animation
        swipe_refresh.setOnRefreshListener {
            getPublicaciones()
        }
        binding.fab.setOnClickListener {
            val intent =Intent(context,CreatePublicationActivity::class.java)
            startActivity(intent)
        }
        publicaciones = ArrayList()
        getPublicaciones()
    }

    private fun getPublicaciones(){
        doAsync {
            val response = PublicacionesTask.getPublicaciones()
            uiThread {
                try {

                    linearLayoutManager = LinearLayoutManager(context)
                    binding.rvPublicaciones.layoutManager = linearLayoutManager
                    publicaciones=response.publicaciones
                    binding.rvPublicaciones.adapter = PublicacionAdapter(publicaciones)
                    binding.rvPublicaciones.scheduleLayoutAnimation()
                    avi.hide()
                    carga.visibility=View.GONE
                    swipe_refresh.isRefreshing = false
                }catch (ex:Exception){

                }

            }
        }
    }
}
