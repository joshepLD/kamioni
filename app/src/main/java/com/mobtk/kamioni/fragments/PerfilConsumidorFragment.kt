package com.mobtk.kamioni.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.mobtk.kamioni.R
import com.mobtk.kamioni.utilities.ViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_perfil_consumidor.*

    /**
 * A simple [Fragment] subclass.
 */
class PerfilConsumidorFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_perfil_consumidor, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        perfilConsumidor()
    }

        fun perfilConsumidor(){

            imgPerfilConsumidor.setImageResource(R.drawable.perfil_consumidor)
            nombreUsuarioConsumidor.setText(R.string.name_consumidor)
            val adaptador = ViewPagerAdapter(childFragmentManager)
            adaptador.addFragment(PayUserFragment(),"Pagos")
            viewPagerConsumidor.adapter = adaptador
            tabPerfilConsumidor.setupWithViewPager(viewPagerConsumidor)
            swipe_refresh.isRefreshing = false
        }
}