package com.mobtk.kamioni.fragments


import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator

import com.mobtk.kamioni.R
import com.mobtk.kamioni.activities.DetallePublicacionProActivity
import com.mobtk.kamioni.databinding.FragmentProveedorBinding

/**
 * A simple [Fragment] subclass.
 */
class ProveedorFragment : Fragment(), Validator.ValidationListener {

    private lateinit var binding: FragmentProveedorBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_proveedor, container, false
        )

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.btnregistrarP.id -> {
                    validator!!.toValidate()
                }
            }
        }
    }

    override fun onValidationSuccess() {
        Toast.makeText(context, "Guardado con éxito", Toast.LENGTH_LONG).show()
        val intent = Intent(context, DetallePublicacionProActivity::class.java)
        context?.startActivity(intent)
    }

    override fun onValidationError() {
        Toast.makeText(context, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }

}
