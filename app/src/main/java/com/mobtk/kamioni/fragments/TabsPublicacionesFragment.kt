package com.mobtk.kamioni.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mobtk.kamioni.R
import com.mobtk.kamioni.utilities.ViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_tabs.*

class TabsPublicacionesFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tabs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUserProfile()


    }
    fun setUserProfile(){
        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFragment(PublicacionesFragment(), "Todas")
        adapter.addFragment(PublicacionesFragment(), "Clientes")
        adapter.addFragment(PublicacionesFragment(), "Provedores")
        viewPagerPublicaciones.adapter = adapter
        tabPublicaciones.setupWithViewPager(viewPagerPublicaciones)

    }
}