package com.mobtk.kamioni.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobtk.kamioni.R
import com.mobtk.kamioni.adapters.CobrosAdapter
import com.mobtk.kamioni.databinding.FragmentCobrosBinding
import com.mobtk.kamioni.models.CobrosModel
import com.mobtk.kamioni.taks.CobrosTask
import com.mobtk.kamioni.taks.PublicacionesTask
import kotlinx.android.synthetic.main.fragment_publicaciones.*
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

class CobrosFragment : Fragment() {

    private lateinit var binding: FragmentCobrosBinding
    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var cobros: MutableList<CobrosModel>
    private var animation : LayoutAnimationController? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_cobros, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(binding.root, savedInstanceState)

        animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation)
        binding.rvCobros.layoutAnimation=animation
        swipe_refresh.setOnRefreshListener {
            getCobros()
        }
        getCobros()
    }

    private fun getCobros(){
        doAsync {
            val response = CobrosTask.getCobros()
            uiThread {
                try {

                    linearLayoutManager = LinearLayoutManager(context)
                    binding.rvCobros.layoutManager = linearLayoutManager
                    cobros=response.cobros
                    binding.rvCobros.adapter = CobrosAdapter(cobros)
                    binding.rvCobros.scheduleLayoutAnimation()
                    avi.hide()
                    carga.visibility= View.GONE
                    swipe_refresh.isRefreshing = false
                }catch (ex:Exception){

                }
            }
        }
    }
}
