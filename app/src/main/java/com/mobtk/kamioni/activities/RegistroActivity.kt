package com.mobtk.kamioni.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mobtk.kamioni.R
import com.mobtk.kamioni.fragments.ProveedorFragment
import com.mobtk.kamioni.utilities.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_registro.*

class RegistroActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(ProveedorFragment(), "Proveedor")
        adapter.addFragment(ProveedorFragment(), "Consumidor")
        viewPagerRegistro.adapter = adapter
        tabsRegistro.setupWithViewPager(viewPagerRegistro)

    }
}
