package com.mobtk.kamioni.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.mobtk.kamioni.R
import com.mobtk.kamioni.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity(), Validator.ValidationListener {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityLoginBinding>(this,
            R.layout.activity_login)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        val validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.btnIniciarSesionLogin.id -> {
                    validator.toValidate()
                }
            }
        }
    }

    override fun onValidationError() {
        Toast.makeText(this, "Dados invalidos", Toast.LENGTH_SHORT).show()

    }

    override fun onValidationSuccess() {
        if(binding.etPassword.text.toString()=="1"){
            val intent = Intent(baseContext,ProvedorHomeActivity::class.java)
            intent.putExtra("hero","SUPERHERO1")
            startActivity(intent)
            finish()
        }
        else if(binding.etPassword.text.toString()=="3"){
            val intent = Intent(baseContext,AdministradorHomeActivity::class.java)
            intent.putExtra("hero","SUPERHERO3")
            startActivity(intent)
            finish()
        }else{
            val intent = Intent(baseContext,ProvedorHomeActivity::class.java)
            intent.putExtra("hero","SUPERHERO2")
            startActivity(intent)
            finish()
        }
    }
}
