package com.mobtk.kamioni.activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.devs.readmoreoption.ReadMoreOption
import com.google.android.gms.ads.AdRequest
import com.mobtk.kamioni.ChatActivity
import com.mobtk.kamioni.R
import com.mobtk.kamioni.adapters.ComentariosAdapter
import com.mobtk.kamioni.models.ComentariosModel
import kotlinx.android.synthetic.main.activity_detalle_publicacion_pro.*


class DetallePublicacionProActivity : AppCompatActivity() {

    var comentarios = ArrayList<ComentariosModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle_publicacion_pro)

        val toolbar: Toolbar = findViewById<Toolbar>(R.id.toolbar)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        detallePublicacion()
        data()

        rvComentarios.layoutManager = LinearLayoutManager(this)
        rvComentarios.adapter = ComentariosAdapter(comentarios)

        btnAddComentario.setOnClickListener {
            if (etComentario.text!!.isEmpty()) {
                Toast.makeText(
                    this@DetallePublicacionProActivity,
                    "Campos vacios", Toast.LENGTH_SHORT
                ).show()

            } else {
                ocultarTeclado()
                addCommentaries()
            }
        }
        imv_ChatPublicacion.setOnClickListener {
            val intent = Intent(baseContext, ChatActivity::class.java)
            intent.putExtra("userid","superhero5")
            intent.putExtra("username","Cyclops")
            intent.putExtra("avatar","https://data-us.cometchat.io/assets/images/avatars/cyclops.png")
            intent.putExtra("status","offline")
            startActivity(intent)
        }
        imv_share.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type="text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.")
            startActivity(Intent.createChooser(shareIntent,"Compartir..."))
        }
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
        adView.visibility= View.VISIBLE

    }

    fun detallePublicacion() {

        var publicacion_titulo: String? = intent.getStringExtra("publicacion_titulo")
        var publicacion_desc: String? = intent.getStringExtra("publicacion_desc")
        var publicacion_img: String? = intent.getStringExtra("publicacion_img")
        var publicacion_fecha: String? = intent.getStringExtra("publicacion_fecha")
        var publicacion_user: String? = intent.getStringExtra("publicacion_user")
        var publicacion_contac: String? = intent.getStringExtra("publicacion_contac")

        tv_tituloPublicacion.text = "$publicacion_titulo"
        tv_fechaPublicacion.text = "$publicacion_fecha"
        tv_userPublicacion.text = "$publicacion_user"
        val requestManager = Glide.with(this)
        val requestBuilder = requestManager.load(publicacion_img)
        requestBuilder.into(imagen_publicacion)


        var readMoreOption = ReadMoreOption.Builder(applicationContext)
            .textLength(150, ReadMoreOption.TYPE_CHARACTER)
            .moreLabel("Más")
            .lessLabel("Menos")
            .moreLabelColor(Color.RED)
            .lessLabelColor(Color.BLUE)
            .labelUnderLine(true)
            .expandAnimation(true)
            .build()
        readMoreOption.addReadMoreTo(tv_descripcionPublicacion, publicacion_desc)
    }

    fun data() {
        for (i in 1..1) {

            comentarios.add(
                ComentariosModel(
                   "https://data-us.cometchat.io/assets/images/avatars/wolverine.png",
                    "Wolverine",
                    "No es muy buena esta oferta"
                )
            )
            comentarios.add(
                ComentariosModel(
                    "https://data-us.cometchat.io/assets/images/avatars/spiderman.png",
                    "Spiderman",
                    "Muuy buena esta oferta"
                )
            )

        }
    }

    private fun addCommentaries() {
        comentarios.add(
            ComentariosModel(
                "https://data-us.cometchat.io/assets/images/avatars/ironman.png",
                "Iron Man",
                "" + etComentario.text.toString()
            )
        )
        etComentario.setText("")
        //lYrv.marginTop()
        rvComentarios.layoutManager = LinearLayoutManager(this)
        rvComentarios.adapter = ComentariosAdapter(comentarios)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun ocultarTeclado() {
        val view = this.currentFocus
        view?.let { v ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(v.windowToken, 0)
        }
    }

}
