package com.mobtk.kamioni.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.MobileAds
import com.mobtk.kamioni.R


class SplashActivity : AppCompatActivity(), Runnable {

    private lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MobileAds.initialize(this, "ca-app-pub-3940256099942544/6300978111")
        supportActionBar?.hide()
        simulaCarga()
    }

    private fun simulaCarga(){
        handler =  Handler()
        handler.postDelayed(this@SplashActivity, 3000)
    }

    override fun run() {

        val intent = Intent(this@SplashActivity, LoginActivity::class.java)
        startActivity(intent)
        finish()

    }
}