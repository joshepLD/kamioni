package com.mobtk.kamioni

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.cometchat.pro.constants.CometChatConstants
import constant.StringContract
import screen.CometChatMessageScreen

class ChatActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        val bundle = Bundle()
        val chatFragment= CometChatMessageScreen()
        bundle.putString(StringContract.IntentStrings.NAME, intent.getStringExtra("username"))
        bundle.putString(StringContract.IntentStrings.AVATAR,intent.getStringExtra("avatar"))
        bundle.putString(StringContract.IntentStrings.UID,intent.getStringExtra("userid"))
        bundle.putString(StringContract.IntentStrings.STATUS,intent.getStringExtra("status"))
        bundle.putString(StringContract.IntentStrings.TYPE, CometChatConstants.RECEIVER_TYPE_USER)
        chatFragment.setArguments(bundle)

        supportFragmentManager.beginTransaction().replace(R.id.chatFrame,chatFragment).commit()

    }
}
