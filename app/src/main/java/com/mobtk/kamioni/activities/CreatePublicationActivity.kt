package com.mobtk.kamioni.activities

import android.app.DatePickerDialog
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.mobtk.kamioni.R
import com.mobtk.kamioni.databinding.ActivityPublicationBinding
import java.util.*


class CreatePublicationActivity : AppCompatActivity(),Validator.ValidationListener {
        val binding by lazy{

            DataBindingUtil.setContentView<ActivityPublicationBinding>(this,R.layout.activity_publication)
        }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val validator = Validator(binding)
        validator.setValidationListener(this)
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val charge_adapter = ArrayAdapter.createFromResource(
            this, R.array.kind_vehicle_charge, android.R.layout.simple_spinner_item
        )
        val pasengers_adapter = ArrayAdapter.createFromResource(
            this, R.array.kind_vehicle_pasengers, android.R.layout.simple_spinner_item
        )
        charge_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        pasengers_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)


        binding.setClickListener {
            when (it!!.id) {
                binding.btnCrearPublicacion.id -> {
                    validator.toValidate()
                }
                binding.etRegreso.id->{
                    val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        // Display Selected date in TextView
                        binding.etRegreso.text=Editable.Factory.getInstance().newEditable("" + dayOfMonth + "/" + month+1 + "/" + year)
                    }, year, month, day)
                    dpd.show()
                }
                binding.etSalida.id->{
                    val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        // Display Selected date in TextView
                        binding.etSalida.text= Editable.Factory.getInstance().newEditable("" + dayOfMonth + "/" + month+1 + "/" + year)
                    }, year, month, day)
                    dpd.show()
                }
            }
        }
        binding.spTipo.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val selectedItem = parent.getItemAtPosition(position).toString()
                if (selectedItem == "Carga") {
                    Toast.makeText(applicationContext,"Carga",Toast.LENGTH_LONG).show()
                    binding.spCarga.adapter=charge_adapter
                    binding.spCantidadPasajeros.visibility=View.GONE
                    binding.spCantidadCarga.visibility=View.VISIBLE
                }else if(selectedItem == "Pasajeros"){
                    Toast.makeText(applicationContext,"Pasajeros",Toast.LENGTH_LONG).show()
                    binding.spCarga.adapter=pasengers_adapter
                    binding.spCantidadPasajeros.visibility=View.VISIBLE
                    binding.spCantidadCarga.visibility=View.GONE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

    }

    override fun onValidationError() {

    }

    override fun onValidationSuccess() {

    }
}
