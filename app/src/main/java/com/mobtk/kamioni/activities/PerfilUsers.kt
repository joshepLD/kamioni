package com.mobtk.kamioni.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.mobtk.kamioni.R
import com.mobtk.kamioni.fragments.PerfilFragment
import kotlinx.android.synthetic.main.activity_perfil_users.*

class PerfilUsers : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perfil_users)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        val mainFragment = PerfilFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame_layout_P, mainFragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
        supportFragmentManager.executePendingTransactions()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
