package com.mobtk.kamioni.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentTransaction
import com.cometchat.pro.core.AppSettings
import com.cometchat.pro.core.CometChat
import com.cometchat.pro.exceptions.CometChatException
import com.cometchat.pro.models.User
import com.google.firebase.messaging.FirebaseMessaging
import com.mobtk.kamioni.R
import com.mobtk.kamioni.fragments.*
import com.mobtk.kamioni.models.Notificacion
import kotlinx.android.synthetic.main.activity_administrador_home.*
import kotlinx.android.synthetic.main.activity_provedor_menu.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class AdministradorHomeActivity : AppCompatActivity() {
    lateinit var toolbar: Toolbar
    lateinit var toolbarText: TextView
    private val region = "us"
    lateinit var UID:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_administrador_home)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        if(null == savedInstanceState) {
            val mainFragment = TabsPublicacionesFragment()
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, mainFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit()
            supportFragmentManager.executePendingTransactions()
            UID = intent.getStringExtra("hero")
            initChat()
        }
        setBottomMenu()
        onToolbarName()
        EventBus.getDefault().register(this)
        val topic = "127147cc5ce18b1_user_"+UID.toLowerCase()
        FirebaseMessaging.getInstance().subscribeToTopic(topic)
    }

    fun initChat(){
        val appSetting =
            AppSettings.AppSettingsBuilder().setRegion(region).subscribePresenceForAllUsers()
                .build()

        CometChat.init(applicationContext, getString(R.string.appID),appSetting ,object : CometChat.CallbackListener<String?>() {
            override fun onSuccess(successMessage: String?) {
                login()
            }

            override fun onError(e: CometChatException) {
                initChat()
            }
        })
    }

    fun setBottomMenu() {
        bottom_navigation_administrador.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_feed -> {
                    supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                        if (it is PublicacionesFragment) {
                        } else {
                            val publicacionesFragment = TabsPublicacionesFragment()
                            supportFragmentManager
                                .beginTransaction()
                                .replace(R.id.frame_layout, publicacionesFragment)
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                .commit()
                        }
                    }
                }
                R.id.action_profile->{
                    supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                        if (it is PerfilFragment) {
                        } else {
                            val perfilFragment = PerfilFragment()
                            supportFragmentManager
                                .beginTransaction()
                                .replace(R.id.frame_layout, perfilFragment)
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                .commit()
                        }
                    }
                }
                R.id.action_message->{
                    supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
                        if (it is ChatListFragment) {
                        } else {
                            val chatFragment = ChatListFragment()
                            supportFragmentManager
                                .beginTransaction()
                                .replace(R.id.frame_layout, chatFragment)
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                .commit()
                        }
                    }
                }
            }
            supportFragmentManager.executePendingTransactions()
            onToolbarName()
            return@setOnNavigationItemSelectedListener true
        }
    }

    private fun onToolbarName(){
        toolbarText = toolbar.findViewById(R.id.tVnameToolbar)
        supportFragmentManager.findFragmentById(R.id.frame_layout)?.let {
            // the fragment exists
            if (it is PublicacionesFragment) {
                supportActionBar?.show()
                toolbarText.text = "Publicaciones"
            }
            if (it is PerfilFragment){
                //  toolbarText.text = "Perfil"
                supportActionBar?.hide()
            }
            if (it is ChatListFragment){
                //  toolbarText.text = "Perfil"
                supportActionBar?.hide()

            }

        }
    }
    private fun login() {
        CometChat.login(UID, getString(R.string.apiKey), object : CometChat.CallbackListener<User>() {
            override fun onSuccess(user: User) {

                Toast.makeText(this@AdministradorHomeActivity, "Login successful", Toast.LENGTH_SHORT).show()

            }
            override fun onError(e: CometChatException) {
                Toast.makeText(this@AdministradorHomeActivity, e.details, Toast.LENGTH_LONG).show()

            }
        })
    }
    @Subscribe
    fun enNotificacionRecibida(event: Notificacion){

    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }
}