package com.mobtk.kamioni.interfaz

import com.mobtk.kamioni.responses.CobrosResponse
import com.mobtk.kamioni.responses.PublicacionesResponse
import retrofit2.Call
import retrofit2.http.GET

import com.mobtk.kamioni.responses.UserPayResponse
interface APIServices {
    @GET("zoneversus/25dc01e38edf37a10bcf921a4a76cbdd/raw/58fef4b2f95e52aa1433fdec3eeabb6c42ea84f6/publicaciones")
    fun getPublicaciones(): Call<PublicacionesResponse>
    @GET("MiguelRamirezAgustin/673967a0e5b1393f4b309c3237b30ed8/raw/03840f83e2149325b2d69c47648925be0fe32a5a/cobrosEmpresa")
    fun getCobros(): Call<CobrosResponse>
    @GET("roimi57/a7adbe347aefbb472fcad845c7221217/raw/b2769c5e452f1c64f2e13b02f1c45c19a30b0632/PagosUsuario")
    fun getPayUser(): Call<UserPayResponse>
}