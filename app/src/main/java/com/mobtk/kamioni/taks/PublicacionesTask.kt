package com.mobtk.kamioni.taks

import com.mobtk.kamioni.interfaz.APIServices
import com.mobtk.kamioni.responses.PublicacionesResponse
import com.mobtk.kamioni.utilities.RetrofitBuilder
import retrofit2.Call

class PublicacionesTask {
    companion object{
        fun getPublicaciones():PublicacionesResponse{
            val call = RetrofitBuilder.getRetrofit().create(
                APIServices::class.java).getPublicaciones().execute()
            val response = call.body() as PublicacionesResponse
            return response
        }
    }
}