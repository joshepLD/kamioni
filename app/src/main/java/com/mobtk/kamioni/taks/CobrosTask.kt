package com.mobtk.kamioni.taks

import com.mobtk.kamioni.interfaz.APIServices
import com.mobtk.kamioni.responses.CobrosResponse
import com.mobtk.kamioni.responses.PublicacionesResponse
import com.mobtk.kamioni.utilities.RetrofitBuilder

class CobrosTask {
    companion object{
        fun getCobros(): CobrosResponse {
            val call = RetrofitBuilder.getRetrofit().create(
                APIServices::class.java).getCobros().execute()
            val response = call.body() as CobrosResponse
            return response
        }
    }
}