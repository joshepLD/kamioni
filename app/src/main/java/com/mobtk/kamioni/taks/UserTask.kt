package com.mobtk.kamioni.taks

import com.mobtk.kamioni.responses.UserPayResponse
import com.mobtk.kamioni.interfaz.APIServices
import com.mobtk.kamioni.utilities.RetrofitBuilder

class UserTask{
    companion object{
        fun getPayUsers(): UserPayResponse {
            val call = RetrofitBuilder.getRetrofit().
                    create(APIServices::class.java).getPayUser().execute()
            val payUser = call.body() as UserPayResponse
            return payUser
        }

    }
}